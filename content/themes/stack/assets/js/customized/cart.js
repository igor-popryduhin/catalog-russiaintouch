

(function (window) {


    Vue.component('cart-item', {
        inheritAttrs: false,
        props: [
            'cart_item',
            'quantity'
        ],

        template: getCartComponent(),
    });


    var vm_cart = new Vue({

        devtools: true,

        delimiters: vue_delimiters,
        el: '#vm_cart',
        data: {
            cost: 0,
            totalAmount: 0,
            deliveryAmount: 0,
            shopcart: [],
            shopcart_empty: true
        },

        mounted: function () {
            this.$nextTick(function () {
                this.loadingCheckout();
            });
        },


        watch: {
            shopcart: function (val, oldVal) {
                if (val.length === 0){
                    this.$data.shopcart_empty = true;
                }  else {
                    this.$data.shopcart_empty = false;
                }
            },
        },

        methods: {

            /**
             * Инициализировать платёж
             */
            pay: function(){

                axios.get('/method/order.create').then((response) => {

                    switch (response.data.error_code) {

                        case 0: {

                            axios.get('/method/order.getRow')
                                .then((response) => {

                                if (response.data.error_code === 0){

                                   let amount = response.data.data.amount;
                                   let description = window.btoa(response.data.data.description);

                                   window.open(`/method/payment.create?amount=${amount}&description=${description}`, '_self');
                                }

                            });

                            swal({
                                title: response.data.title,
                                text: response.data.msg,
                                icon: response.data.type,
                                timer: 5000
                            }).then(() => {

                            });

                            break;
                        }

                        case 17: {
                            swal({
                                text: lang.translate('message', 'user_authorization_required'),
                                icon: "warning",
                                buttons: {
                                    cancel: {
                                        text: lang.translate('main', 'btn_cancel'),
                                        value: null,
                                        visible: true,
                                        className: "",
                                        closeModal: true,
                                    },
                                    confirm: {
                                        text: lang.translate('account', 'btn_authorization'),
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: true
                                    }
                                }
                            }).then((isConfirm) => {
                                if (isConfirm) {
                                    window.open(`https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx3965d6a7bac6992a&redirect_uri=${location.origin}/method/WeChatCallback&response_type=code&scope=snsapi_userinfo&state=wechat`, '_self');
                                }
                            });
                            break;
                        }

                        default: {
                            swal({
                                title: response.data.title,
                                text: response.data.msg,
                                icon: response.data.type
                            });
                        }
                    }

                });

            },

            /**
             * Инициализация "промо"
             */
            promoinit: function(){
                swal({
                    title: 'Промо-акция',
                    text: response.data.msg,
                    icon: response.data.type,
                    timer: 5000
                }).then(() => {

                });
            },

            /**
             *
             * @param id
             * @param target
             * @param index
             */
            onQuantity: function (id, target, index) {

                switch (target) {
                    case "inc": {
                        this.$data.shopcart[index].quantity++;
                        break;
                    }

                    case "dec": {
                        this.$data.shopcart[index].quantity--;
                        break;
                    }
                }

                if (this.$data.shopcart[index].quantity < 1) {
                    this.$data.shopcart[index].quantity = 1;
                }

                if (this.$data.shopcart[index].quantity > 20) {
                    this.$data.shopcart[index].quantity = 20;
                }

                calc();

                console.debug([id, target, index])
            },

            /**
             * Загрузка элементов корзины
             */
            loadingCheckout: function () {
                let url = new URL(location.origin + '/method/cart.getCheckout');

                axios.get(url).then((response) => {

                    if (response.data.error_code === 0) {

                        /** @namespace response.data.total_amount */

                        this.$data.cost = response.data.total_amount;
                        this.$data.totalAmount = response.data.total_amount;
                        this.$data.shopcart = response.data.items;

                    } else {
                        swal({
                            title: json.title,
                            text: json.msg,
                            icon: json.type,
                            buttons: true
                        });
                    }
                });
            },


            /**
             * Удалить один товар из корзины
             *
             * @param index
             */
            remove: function (index) {
                let id = this.$data.shopcart[index].id;
                axios.get(`/method/cart.remove?id=${id}`).then((response) => {

                    if (response.data.error_code === 0) {

                        swal({
                            title: response.data.title,
                            text: response.data.msg,
                            icon: response.data.type,
                            buttons: true
                        });

                        this.$data.shopcart.splice(index, 1);
                    } else {
                        swal({
                            title: response.data.title,
                            text: response.data.msg,
                            icon: response.data.type,
                            buttons: true
                        });
                    }

                });
            }
        }
    });

    vm_cart.$on('changes-quantity-cart', function () {
        console.debug(this);
    });


    /**
     * Подписка на загрузку данных корзины
     */
    vm_cart.$on('loadingCheckout', function () {
        cartCountCalc();
    });



    function calc() {
        let summ = 0;
        vm_cart.$data.shopcart.forEach(function (e, i, a) {
            e.amount = e.quantity * e.price;
            summ = summ + e.amount;
        });

        vm_cart.$data.totalAmount = summ;
    }
})(window);
