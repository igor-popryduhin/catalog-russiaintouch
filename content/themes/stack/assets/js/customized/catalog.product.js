(function () {

    let lang = Cookie.get('lang', 'zh');

    let current_category_parent_id = LStorage.read('current_category_parent_id', 0);

    Vue.component('product-item', {
        inheritAttrs: false,
        props: [
            'id',
            'title',
            'image',
            'description',
            'price',
            'old_price',
        ],

        template: productItem(),
    });


    /**
     * Категории продуктов
     * @type {Vue}
     */
    let vm_category = new Vue({

        delimiters: vue_delimiters,
        el: '#vm_category',
        data: {
            category: []
        },


        mounted: function () {
            this.$nextTick(function () {
                this.loadingCategory();
            });
        },

        methods: {

            /**
             * Загрузка категорий
             *
             * @param category_id
             * @param offset
             * @param count
             * @param lang
             */
            loadingCategory: function (category_id = 0, offset = 0, count = 150, lang = 'zh') {

                let url = location.origin + `/method/category.navigate?id=${category_id}&offset=${offset}&count=${count}&lang=${lang}`;

                axios.get(url).then((response) => {

                    if (response.data.items.length === 0) return true;

                    this.$emit('loadingCategory', response.data.id);

                    this.$data.category = response.data.items;
                });
            },

            /**
             * Навигация по меню
             *
             * @param category_id
             * @param parent_id
             */
            navigate: function (category_id, parent_id = 0) {
                this.loadingCategory(parent_id, 0, 100, lang);
                this.$emit('navigate', {category_id, parent_id});
            }
        }
    });


    /**
     * Подписка на событие навигации
     */
    vm_category.$on('navigate', function (obj) {
        vm_catalog.getProducts(obj.category_id, 0, 100, lang);
    });


    /**
     * Подписка на загрузку категорий
     */
    vm_category.$on('loadingCategory', function (current_category_parent_id) {
        console.log(current_category_parent_id);
    });


    /**
     * Каталог
     *
     * @type {Vue}
     */
    let vm_catalog = new Vue({

        delimiters: vue_delimiters,
        el: '#vm_catalog',
        data: {
            products: []
        },


        mounted: function () {
            this.$nextTick(function () {
                this.getProducts(current_category_parent_id, 0, 100, lang)
            })
        },

        methods: {

            /**
             * Загрузка всех товаров с возможностью загрузки подмножеств
             *
             * @param category_id
             * @param offset
             * @param count
             * @param lang
             */
            getProducts: function (category_id = 0, offset = 0, count = 150, lang = 'zh') {
                axios
                    .get(location.origin + `/method/product.getProducts?category_id=${category_id}&offset=${offset}&count=${count}&lang=${lang}`)
                    .then((response) => {

                        response.data.items.forEach(function (e, i, a) {
                            a[i].description = shorten(e.description, 250, ' ...')
                        });

                        this.$data.products = response.data.items;
                    });
            },


            /**
             * Удалить все продукты из корзины
             */
            removeAll: function () {
                this.$data.products = [];
            },


            /**
             * Добавление товара в корзину
             * @param id
             */
            addToCart: function (id) {

                let obj = {};
                obj.product_id = id;

                axios.post("/method/cart.add", obj).then((response) => {
                    swal({
                        title: response.data.title,
                        text: response.data.msg,
                        icon: response.data.type,
                        button: false,
                        timer: 2000
                    }).then(function () {
                        cartCountCalc();
                    });
                });
            }
        }
    });


    /**
     *
     * @param text
     * @param maxLength
     * @param delimiter
     * @param overflow
     * @returns {*}
     */
    function shorten(text, maxLength, delimiter, overflow) {
        delimiter = delimiter || "&hellip;";
        overflow = overflow || false;
        var ret = text;
        if (ret.length > maxLength) {
            var breakpoint = overflow ? maxLength + ret.substr(maxLength).indexOf(" ") : ret.substr(0, maxLength).lastIndexOf(" ");
            ret = ret.substr(0, breakpoint) + delimiter;
        }
        return ret;
    }




})(window);
