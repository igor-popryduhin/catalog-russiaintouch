
+function () {

    /**
     *
     */
    $(document).ready(function() {
        $(`#setting-language [value="${Cookie.get('lang', 'zh')}"]`).attr('selected', true);
    });


    /**
     *
     */
    $('#setting-language').change(function (e) {
        Cookie.set('lang', $(this).val(), {
            path: "/"
        });
    });


}();