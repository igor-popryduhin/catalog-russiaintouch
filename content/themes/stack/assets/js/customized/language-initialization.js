

var lang = new LangPackage('/general/language.getPackage');
lang.registerHandler(
    'onLoaded', function () {
        lang.autoTranslate();
    });

let packages = [];

packages.push('main');
packages.push('catalog');
packages.push('message');
packages.push('shop');
packages.push('account');
packages.push('profile');

lang.load(packages.join(','), Cookie.get('lang', 'zh'), 'cms');