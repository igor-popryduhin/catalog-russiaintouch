/**
 *  Authorization using a mobile phone
 */

+function(){

    const input_code_mask = "9999";
    const input_phone_mask = "9999";


    let inputCode = $('<input>')
        .attr('type', 'text')
        .attr('class', 'form-control international-inputmask')
        .attr('data-validation-match-match', 'phone');

    inputCode = $(inputCode).inputmask(input_code_mask)[0];

    /**
     * Check code
     */
    let confirmCodeDialog = function(){

        swal({
            text: lang.translate('account', 'enter_code_confirm'),
            content: inputCode,

            buttons: {
                cancel: {
                    text: lang.translate('main', 'btn_cancel'),
                    value: "cancel",
                    visible: true,
                    className: "btn-danger",
                    closeModal: true,
                },
                confirm: {
                    text: lang.translate('account', 'btn_confirm'),
                    value: "confirm",
                    visible: true,
                    className: "btn-primary",
                    closeModal: false
                }
            },

            closeOnClickOutside: false


        }).then(function (result) {

            switch (result) {
                case "cancel": {
                    throw null;
                }

                case "confirm": {
                    return fetch(`/SMSVerification.checkSMSCode?code=${$(inputCode).val()}`);
                }
            }

        }).then(function (result) {
            return result.json();
        }).then(function (json) {

            if (json.error_code === 0){
                swal({
                    title: json.title,
                    text: json.msg,
                    icon: json.type,
                    button: false,
                    timer: 4000
                });
            } else {
                swal({
                    title: json.title,
                    text: json.msg,
                    icon: json.type,
                    button: false,
                    timer: 1500
                }).then(function () {
                    $(inputCode).val('');
                    confirmCodeDialog()
                });
            }

        })
    };


    $('#phone').on('click', function (e) {

        // <input type="text" class="form-control international-inputmask" id="international-mask" placeholder="International Phone Number">
        // <input type="email" name="email2" data-validation-match-match="email" class="form-control" required="" aria-invalid="true">

        let inputPhone = $('<input>')
            .attr('type', 'text')
            .attr('class', 'form-control international-inputmask')
            .attr('data-validation-match-match', 'phone')
            .val(LStorage.read('tel', ''));

        inputPhone = $(inputPhone).inputmask("+9 (999) 999-9999")[0];


        swal({
            text: lang.translate('account', 'enter_phone_number'),
            content: inputPhone,
            buttons: {
                i_have_a_code: {
                    text: lang.translate('account', 'i_have_a_code'),
                    value: "i_have_a_code",
                    visible: true,
                    className: "btn-danger",
                    closeModal: true,
                },
                btn_get_code: {
                    text: lang.translate('account', 'btn_get_code'),
                    value: "btn_get_code",
                    visible: true,
                    className: "btn-primary",
                    closeModal: false
                }
            },
            closeOnClickOutside: false


        }).then(function (result) {
            if (!result) throw null;

            switch (result) {
                case "i_have_a_code": {

                    confirmCodeDialog();

                    break;
                }

                case "btn_get_code": {
                    LStorage.write('tel', $(inputPhone).val());
                    return fetch(`/SMSVerification.sendSMS?phone=${Base64.encode($(inputPhone).val())}`);
                }

                default: throw null
            }

        }).then(results => {

            return results.json();

        }).then(function (json) {

            if (json.error_code === 0){

                confirmCodeDialog();

            } else {
                swal({
                    title: json.title,
                    text: json.msg,
                    icon: json.type,
                    timer: 4000
                }).then()
            }

        })
    });


}();