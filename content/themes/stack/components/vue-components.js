function getCartComponent() {
    return `
 <div class="row" >
    <div class="col-12" style="padding-left: 5px; padding-right: 5px">
        <div class="card text-left" style="margin-bottom: 0.475rem;">
            <div class="card-header bg-transparent">
                <h4 class="card-title" style="color: #ff111b">{{cart_item.title}}</h4>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 p-0">
                            <img class="" :src="cart_item.image" alt="" style="max-width: 120px">
                        </div>
                        <div class="col-8 p-0">
                            <ul style="list-style-type: none;font-size: larger;color: #404e67;padding-left: 20px">
                                <li> {{cart_item.amount}} ₽</li>
                                <li>Кол-во: {{cart_item.quantity}} шт.</li>
                                <li></li>
                            </ul>
                        </div>
                        <fieldset class="ts-pos">
                            <div class="input-group input-group-sm bootstrap-touchspin">
                                <div class="input-group-prepend">
                                    <button type="button" name="is-dec" class="btn btn-icon btn-pure secondary" v-on:click="$emit('quantity-dec', quantity)">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>

                                <span class="ts-value">{{ quantity }}</span>

                                <div class="input-group-append">
                                    <button type="button" name="is-inc" class="btn btn-icon btn-pure secondary" v-on:click="$emit('quantity-inc', quantity)">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                     
                        <button type="button" name="is-inc" class="btn btn-icon btn-pure secondary" style="
                            position: absolute;
                            right: 12px;
                            bottom: 8px;
                            color: #ff5169;"
                            v-on:click="$emit('remove')"
                        ><i class="fa fa-trash"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `
}

/**
 * Компонент оформления заказа
 *
 * @returns {string}
 */
function checkout() {
    return `
        <div class="row">
            <div class="col-12" style="padding-left: 5px; padding-right: 5px">
                <div class="card" style="margin-bottom: 0.475rem;">
                    <div class="card-content">
                        <div class="card-body">
                            <h3>Оформление заказа</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `
}


function productItem() {
    return `<div class="col-12">
    <div class="card">
        <div class="card-content">
            <img class="card-img-top img-fluid" v-bind:src="image">
            <div class="card-body">
                <h4 class="card-title" style="cursor: pointer; color: #ff8f00">{{ title }}</h4>
                <div v-html="description"></div>
            </div>
        </div>
        <div class="card-footer text-muted mt-2">
            <span class="float-left">
                <del class="grey" v-if="old_price > 0">{{ old_price }} ₽</del>
                <span class="ml-1">{{ price }} ₽</span>
            </span>
            <button class="btn btn-outline-teal float-right disabled"
                v-on:click="$emit('add_to_cart')"
            >
                <i class="fa fa-cart-plus"></i>
            </button>
        </div>
    </div>
</div>
                
    `;
}