/**
 * @constructor
 */
Category = function () {
    var self = this;
    var baseUrl = location.origin;
    this.handlers = {};
    this.events = [
        'onComplete'
    ];


    /**
     * @param category_id
     * @param lang
     */
    this.getChildren = function (category_id, lang) {

        let url = new URL(baseUrl + '/data/category.getChildren');

        url.searchParams.set('category_id', category_id);
        url.searchParams.set('lang', lang);

        httpGet(url.href, function (response) {
            self.onComplete('getChildren', response);
        })
    };


    /**
     * @param category_id
     * @param lang
     */
    this.getParents = function (category_id, lang) {

        let url = new URL(baseUrl + '/data/category.getParents');

        url.searchParams.set('category_id', category_id);
        url.searchParams.set('lang', lang);

        httpGet(url.href, function (response) {
            self.onComplete('getParents', response);
        })
    };

    /**
     * =================================================
     * Events
     * =================================================
     */
    /**
     * @param method
     * @param response
     */
    this.onComplete = function (method, response) {
        for (var i = 0, l = this.handlers.onComplete.length; i < l; i++) {
            this.handlers.onComplete[i](method, response);
        }
    };


    /**
     * @param type
     * @param func
     */
    this.registerHandler = function (type, func) {
        if (this.events.indexOf(type) === -1) throw "Invalid Event!";
        if (this.handlers[type]) {
            this.handlers[type].push(func);
        } else {
            this.handlers[type] = [func];
        }
    };


    /**
     * @param url
     * @param callback function
     * @param async
     */
    function httpGet(url, callback, async = true) {

        let settings = {
            "async": async,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "headers": {
                "cache-control": "no-cache"
            }
        };

        $.ajax(settings).done(function (response) {
            callback(response);
        });
    }


    /**
     * @param url
     * @param data mixed
     * @param callback
     */
    function httpPost(url, data, callback) {

        let settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "POST",
            "headers": {
                "cache-control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data",
            "data": data,
            "dataType": "json"
        };

        $.ajax(settings).done(function (response) {
            callback(response);
        });
    }
};










