/**
 * LStorage
 */
class LStorage{

    /**
     * @param key
     * @param def
     */
    static read(key, def = false){
        if (localStorage.hasOwnProperty(key)){
            return localStorage.getItem(key);
        }
        return def;
    }

    /**
     * @param key
     * @param value
     */
    static write(key, value) {
        localStorage.setItem(key, value)
    }


    /**
     * @param key
     * @returns {boolean}
     */
    static has(key){
        return !!localStorage.hasOwnProperty(key);
    }
}