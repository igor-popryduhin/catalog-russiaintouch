var vue_delimiters = ['${', '}'];

var vm = new Vue({
    delimiters: vue_delimiters,

    el: '#app',

    data: {
        cart_count: 0
    },

    mounted: function () {
        this.$nextTick(function () {

            getCartCount((count) => {
                this.$data.cart_count = count;
            });

        })
    },

    methods: {

    }
});


/**
 * Количество товаров в корзине
 */
function cartCountCalc() {
    axios.get('/method/cart.count')
        .then((response) => {
            vm.$data.cart_count = response.data.count;
        });
}






