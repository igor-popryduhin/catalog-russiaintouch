/**
 *
 */
class LangPackage {
    /**
     * @param url_api
     */
    constructor(url_api) {
        this._cache = true;
        this._api = location.origin + url_api;
        this._packages = {};
        this._handlers = {};
        this._events = [
            'onLoaded'
        ];
    }

    /**
     * @param value
     */
    set cache(value) {
        this._cache = value;
    }

    /**
     * @returns {*}
     */
    get self() {
        return this._self;
    }

    set self(value) {
        this._self = value;
    }

    get events() {
        return this._events;
    }

    set events(value) {
        this._events = value;
    }

    get handlers() {
        return this._handlers;
    }

    set handlers(value) {
        this._handlers = value;
    }

    /**
     * @returns {*|null}
     */
    get packages() {
        return this._packages;
    }

    /**
     * @param value
     */
    set packages(value) {
        this._packages = value;
    }

    /**
     * @param packages
     * @param lang
     * @param env
     */
    load(packages, lang, env = 'Cms') {

        let url = `${this._api}?env=${env}&packages=${packages}&lang=${lang}`;

        let _this = this;


        $.getJSON(url, function (json) {
            _this.packages = json;
            _this.onLoaded();
        });
    }


    /**
     * @param package_name
     * @param key
     */
    translate(package_name, key) {
        let item = this.packages.items[package_name];
        if (typeof item !== "undefined"){
            return this.packages.items[package_name].translated_content[key];
        }
        return 'Language pack file not found!';
    }


    /**
     * Event loaded
     */
    onLoaded() {
        for (let i = 0, l = this.handlers.onLoaded.length; i < l; i++) {
            this.handlers.onLoaded[i]();
        }
    };


    /**
     * @param type
     * @param func
     */
    registerHandler(type, func) {
        if (this.events.indexOf(type) === -1) throw "Invalid Event!";
        if (this.handlers[type]) {
            this.handlers[type].push(func);
        } else {
            this.handlers[type] = [func];
        }
    };


    /**
     * Find and translate content
     */
    autoTranslate(selector = '[data-lang-key]') {
        let _this = this;
        $(selector).each(function (a, c, d) {
            let lang_package = $(this).attr('data-lang-package');
            let data_lang_key = $(this).attr('data-lang-key');

            $(this).text(_this.translate(lang_package, data_lang_key))
        })
    }
}