<?php

use Engine\Helper\PHPOptions;

return [
    'NAV_BAR_TITLE'     => 'Russia in touch',
    'HTTP_CATALOG'      => 'https://russiaintouch.com',
    'DEFAULT_LANGUAGE'  => 'ru',


    'DIR_IMAGE'         => '/content/images',
    'DIR_IMG_MINIATURES'=> '/content/img/miniatures' . DS . strtolower(ENV),
    'DEFAULT_IMAGE'     => '/content/default/image/no-image.png',

    /**
     * Twig
     */
    'TWIG_CACHE'        => '/twig/cache'
];