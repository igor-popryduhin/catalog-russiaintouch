<?php


namespace Engine;

/**
 * Class DataRepository
 * @package Engine
 */
class DataRepository
{
    /**
     * @var array
     */
    static private $data = [];


    /**
     * @param string $key
     * @param        $value
     */
    static public function setValue(string $key, $value)
    {
        self::$data[$key] = $value;
    }



    /**
     * @param string $key
     * @param bool   $default
     *
     * @return bool
     */
    static public function getValue(string $key, $default = false)
    {
        if (isset(self::$data[$key]) || !empty(self::$data[$key])){
            return self::$data[$key];
        }

        return $default;
    }



    /**
     * @param string $key
     *
     * @return bool
     */
    static public function hasValue(string $key): bool
    {
        return isset(self::$data[$key]) ? true: false;
    }



    /**
     * @return array
     */
    public static function getData():array
    {
        return self::$data;
    }



    /**
     * @param array $data
     */
    public static function setData(array $data):void
    {
        self::$data = $data;
    }
}