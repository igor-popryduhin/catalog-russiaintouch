<?php

namespace Engine;

use Engine\Core\Config\Config;
use Engine\Core\Database\SafeMySQL;
use Engine\DI\DI;

/**
 * Class Model
 * @package Engine
 */
abstract class AbstractModel
{
    /**
     * @var DI
     */
    protected $di;
    /**
     * @var Config
     */
    protected $config;
    /**
     * @var Load
     */
    protected $load;
    /**
     * @var SafeMySQL
     */
    protected $db;

    /**
     * Model constructor.
     * @param $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->db = $this->di->get('db');
        $this->config = $this->di->get('config');
        $this->load = $this->di->get('load');
    }



    /**
     * @return SafeMySQL
     */
    protected function db():SafeMySQL
    {
        return $this->db;
    }
}