<?php

return [

    "DEFAULT_LANGUAGE"      => "zh",
    "THEME"                 => "stack",
    "DIR_IMAGE"             => "content/images",
    "DIR_IMG_MINIATURES"    => "/content/img/miniatures",

    "TWIG_CACHE"            => "/twig/cache",

    "PROFILE_IMG_DEFAULT"   => "/content/assets/image/default/default-avatar.png"
];
