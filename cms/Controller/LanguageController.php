<?php


namespace Cms\Controller;


use Engine\AbstractController;
use Engine\Core\Database\ExceptionSafeMySQL;
use Engine\Core\DataRepository\Data;
use Engine\DI\DI;
use Engine\Helper\Cookie;
use Engine\Helper\Network;
use Exception;
use General\Model\Language\Language;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class LanguageController extends AbstractController
{
    /**
     * @var Language
     */
    private $language;



    /**
     * LanguageController constructor.
     *
     * @param DI $di
     *
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
        $this->language = $this->load->model('Language', false, 'General');
    }



    /**
     * @throws ExceptionSafeMySQL
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function select()
    {
        $this->data::setValue('language', $this->language->getRows());
        $this->data::setValue('callback', $this->request->get('callback', '/home'));

        $this->twig->load('language.twig');
        echo $this->twig->render('language.twig', Data::getData());
    }



    /**
     * Set language
     */
    public function set()
    {
        $url = $this->request->get('callback', '/home');
        $lang = $this->request->get('lang', 'zh');
        Cookie::set('lang', $lang);
        Network::location($url);
    }
}