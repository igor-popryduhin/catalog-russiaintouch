<?php


namespace Cms\Controller\Account;


class SettingController extends AccountController
{

    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Engine\Core\Database\ExceptionSafeMySQL
     */
    public function setting()
    {
        $this->twig->load('account/setting.twig');
        $this->data::setValue('language', $this->language->getRows());
        echo $this->twig->render('account/setting.twig', $this->data::getData());
    }
}