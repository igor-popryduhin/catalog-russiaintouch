<?php

namespace Cms\Controller\Account;



use Cms\Controller\PublicController;
use Cms\Model\Order\Order;
use Engine\Core\Config\Config;
use Engine\DI\DI;

/**
 * Class ProfileController
 * @package Cms\Controller\Account
 */
class ProfileController extends PublicController
{

    /**
     * @var Order
     */
    private $order;



    /**
     * ProfileController constructor.
     *
     * @param DI $di
     *
     * @throws \Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
        $this->order = $this->load->model('Order');


        $this->data::setValue('profile', $this->customer->get($this->getCustomerId()));
    }



    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Exception
     */
    public function profile()
    {

        if (!$this->data::hasValue('profile')){
            $profile = new \stdClass();
            $profile->image = Config::item('PROFILE_IMG_DEFAULT');

            $this->data::setValue('profile', $profile);
        }

        $this->twig->load('account/profile.twig');
        echo $this->twig->render('account/profile.twig', $this->data::getData());
    }



    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Engine\Core\Database\ExceptionSafeMySQL
     */
    public function orders()
    {
        $this->data::setValue('orders', $this->order->getRows($this->getCustomerId()));
        $this->twig->load('account/user-orders.twig');
        echo $this->twig->render('account/user-orders.twig', $this->data::getData());
    }
}