<?php


namespace Cms\Controller;


use Engine\DI\DI;

use Engine\Helper\Network;
use Exception;
use General\Model\Language\Language;


/**
 * Class IndexController
 * @property mixed model
 * @package Cms\Controller
 */
class IndexController extends CmsController
{

    /** @var int */
    public $user_group_id;
    /** @var int */
    public $language_code;


    /**
     * IndexController constructor.
     *
     * @param DI $di
     *
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
    }



    /**
     * @throws \Exception
     */
    public function index()
    {
        Network::location('/home');
    }
}