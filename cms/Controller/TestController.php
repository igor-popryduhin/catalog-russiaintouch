<?php
/**
 * Created by PhpStorm.
 * User: igorp
 * Date: 26.03.2019
 * Time: 15:38
 */

namespace Cms\Controller;


use Engine\AbstractController;
use Engine\Core\Config\Config;
use Engine\Core\Database\ExceptionSafeMySQL;
use Engine\DI\DI;
use Exception;
use YandexCheckout\Client;
use YandexCheckout\Common\Exceptions\ApiException;
use YandexCheckout\Common\Exceptions\BadApiRequestException;
use YandexCheckout\Common\Exceptions\ForbiddenException;
use YandexCheckout\Common\Exceptions\InternalServerError;
use YandexCheckout\Common\Exceptions\NotFoundException;
use YandexCheckout\Common\Exceptions\ResponseProcessingException;
use YandexCheckout\Common\Exceptions\TooManyRequestsException;
use YandexCheckout\Common\Exceptions\UnauthorizedException;
use YandexCheckout\Model\Payment;

/**
 * Class TestController
 * @package Cms\Controller
 */
class TestController extends AbstractController
{
    /**
     * TestController constructor.
     *
     * @param DI $di
     *
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
    }



    /**
     * @throws Exception
     */
    public function createPayment()
    {

        $client = new Client();
        $client->setAuth(Config::item('SHOP_ID', 'yandex.kassa'), Config::item('SECRET_KEY', 'yandex.kassa'));

        $idempotenceKey = uniqid('', true);
        try {
            $response = $client->createPayment(
                [
                    'amount' => [
                        'value' => 5.0,
                        'currency' => 'RUB',
                    ],
                    'confirmation' => [
                        'type' => 'redirect',
                        'return_url' => 'https://dev.russiaintouch.com/yandex.receive',
                        'locale' => 'en_US'
                    ],
                    'capture' => true,
                    'description' => 'Заказ №1'
                ],
                $idempotenceKey
            );



            print $response->getStatus();


        } catch (BadApiRequestException $e) {
            echo $e->getMessage();
        } catch (ForbiddenException $e) {
            echo $e->getMessage();
        } catch (InternalServerError $e) {
            echo $e->getMessage();
        } catch (NotFoundException $e) {
            echo $e->getMessage();
        } catch (ResponseProcessingException $e) {
            echo $e->getMessage();
        } catch (TooManyRequestsException $e) {
            echo $e->getMessage();
        } catch (UnauthorizedException $e) {
            echo $e->getMessage();
        } catch (ApiException $e) {
            echo $e->getMessage();
        }




    }



    public function receive()
    {
        $this->logger->debug('receiving', $this->request->request);
    }



    public function x()
    {

    }



    /**
     * @throws ExceptionSafeMySQL
     */
    public function ping()
    {
        $this->db->query("insert into setting set `key` = 'android', `value` = TIMESTAMP(CURRENT_TIMESTAMP()) ON DUPLICATE KEY UPDATE `value` = TIMESTAMP(CURRENT_TIMESTAMP())");
    }
}