<?php


namespace Cms\Controller\Catalog\Product;


use Cms\Controller\Catalog\CatalogController;
use Cms\Model\Catalog\Product\Product;
use Engine\Core\Database\ExceptionSafeMySQL;
use Engine\DI\DI;
use Engine\Helper\Helper;
use Exception;
use stdClass;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ProductController
 * @package Cms\Controller\Catalog\Product
 */
class ProductController extends CatalogController
{
    /**
     * @var Product
     */
    private $product;



    /**
     * ProductController constructor.
     *
     * @param $di
     *
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);

        $this->product = $this->load->model('Product', 'Catalog/Product');
    }



    /**
     * @param int $product_id
     *
     * @throws ExceptionSafeMySQL
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @deprecated
     */
    public function d($product_id = 1)
    {
        $obj = new stdClass();
        $obj->details = (object)$this->product->getRow($product_id, $this->lang);

        $this->data::setValue('product', $obj->details);

        $this->twig->load('catalog/product/details.twig');
        echo $this->twig->render('catalog/product/details.twig', $this->data::getData());
    }



    /**
     * @throws ExceptionSafeMySQL
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function details()
    {
        $product_id = $this->request->get('product_id', false);
        $product = $this->product->getRow($product_id, $this->lang);

        $this->data::setValue('product', $product) ;

        $this->twig->addGlobal('HEADER_TITLE', $product['title']);
        $this->twig->load('catalog/product/details.twig');
        echo $this->twig->render('catalog/product/details.twig', $this->data::getData());
    }



    /**
     * @throws ExceptionSafeMySQL
     */
    public function getRows()
    {
        $offset = $this->request->mixed('offset', 0);
        $count = $this->request->mixed('count', 10);

        $outObj = new stdClass();
        $outObj->error_code = 0;
        $outObj->count = $this->product->count();
        $outObj->items = $this->product->getRows($offset, $count, $this->lang);

        Helper::echoJsonUtf8($outObj);
        exit();
    }



    /**
     * @throws ExceptionSafeMySQL
     */
    public function getProducts()
    {
        $category_id = (int)$this->request->mixed('category_id', 0);
        $offset = (int)$this->request->mixed('offset', 0);
        $count = (int)$this->request->mixed('count', 100);

        if ($category_id == 0) {
            $category_id = 116;
        }

        $outObj = new stdClass();
        $outObj->error_code = 0;
        $outObj->count = $this->product->count();
        $outObj->items = $this->product->get($category_id, $offset, $count, $this->lang);

        Helper::echoJsonUtf8($outObj);
        exit();
    }
}