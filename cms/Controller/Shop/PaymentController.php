<?php


namespace Cms\Controller\Shop;


use Cms\Controller\ProtectedController;
use Cms\Controller\PublicController;
use Engine\AbstractController;
use Engine\Core\Config\Config;
use Engine\Helper\Network;
use Exception;
use YandexCheckout\Client;
use YandexCheckout\Common\Exceptions\ApiException;
use YandexCheckout\Common\Exceptions\BadApiRequestException;
use YandexCheckout\Common\Exceptions\ForbiddenException;
use YandexCheckout\Common\Exceptions\InternalServerError;
use YandexCheckout\Common\Exceptions\NotFoundException;
use YandexCheckout\Common\Exceptions\ResponseProcessingException;
use YandexCheckout\Common\Exceptions\TooManyRequestsException;
use YandexCheckout\Common\Exceptions\UnauthorizedException;

/**
 * Class PaymentController
 * @package Cms\Controller\Shop
 */
class PaymentController extends ProtectedController
{


    /**
     * Создать платёж
     * @throws Exception
     */
    public function create()
    {
        $amount = (double)$this->request->mixed('amount');
        $description = base64_decode($this->request->mixed('description'));

        $client = new Client();
        $client->setAuth(

            Config::item('SHOP_ID', 'yandex.kassa'),
            Config::item('SECRET_KEY', 'yandex.kassa')

        );

        $idempotenceKey = uniqid('', true);
        try {
            $response = $client->createPayment(
                [
                    'amount' => [
                        'value' => $amount,
                        'currency' => 'RUB',
                    ],

                    'confirmation' => [
                        'type' => 'redirect',
                        'return_url' => 'https://dev.russiaintouch.com/profile.orders',
                        'locale' => 'ru_RU'
                    ],

                    'capture' => true,

                    'description' => $description
                ],
                $idempotenceKey
            );



            print $response->getStatus();


            Network::location($response->confirmation->offsetGet('confirmation_url'));




        } catch (BadApiRequestException $e) {
            echo $e->getMessage();
        } catch (ForbiddenException $e) {
            echo $e->getMessage();
        } catch (InternalServerError $e) {
            echo $e->getMessage();
        } catch (NotFoundException $e) {
            echo $e->getMessage();
        } catch (ResponseProcessingException $e) {
            echo $e->getMessage();
        } catch (TooManyRequestsException $e) {
            echo $e->getMessage();
        } catch (UnauthorizedException $e) {
            echo $e->getMessage();
        } catch (ApiException $e) {
            echo $e->getMessage();
        }

    }
}